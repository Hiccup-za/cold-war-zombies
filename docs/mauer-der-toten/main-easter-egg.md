# Main Easter Egg

1. Turn on the Power
1. Get Microwave
1. Build Klaus
1. Get Black light
1. Get Safe Numbers - Garment Factory
    - Top of doorway next to light
    - Blackboard to thr right of doorway
    - Under "I can end this"
1. Get Safe Numbers - Service Passage
    - Above dooway
    - Under big pipe
    - In hallway above box
    - Next room near hallway number
1. Get Safe Numbers - Grocery Store
    - Left side of door left of beer poster
    - Booze shelf
    - Corner of room above big bags
1. Get Wonder Weapon
1. Open Secret Base
1. Get three containers and trap
1. Fill three containers
1. Build Helmet
    - Radio tower, starting area
    - Transformer box, Ghost train area
    - Radio, electronics store
1. Upgrade Klaus
1. Stop Train
    - Warhead from briefcase
    - Floppy disk
1. Place Warhead
1. Use Floppy disk 1st time
    - Kill Elites
    - Craft and collide uraniam
    - Take clean uraniam back
1. Prepare for Boss Fight
1. Use Floppy disk 2nd time
    - Kill Elites
    - Craft and collide uraniam
    - Take clean uraniam back
1. Boss fight
