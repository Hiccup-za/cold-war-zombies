# Mauer Der Toten

## Main Easter Egg

- [Main Easter Egg](main-easter-egg.md)

## Side Easter Eggs

- [Aether Tool Upgrade](aether-tool.md)
- [Bunny Disco Easter Egg](bunny-disco.md)
- [Klaus Upgrades](klaus-upgrades.md)
- [Music Easter Egg](music-easter-egg.md)
- [Wonder Weapon](wonder-weapon.md)
