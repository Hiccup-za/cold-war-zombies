---
sidebar: auto
---

# Stylesheet

## Badges

<colorBadge text="Playstation 4" type="ps4"/>
<colorBadge text="Xbox One" type="xbox"/>
<colorBadge text="Nintendo Switch" type="switch"/>
<colorBadge text="Apple Arcade" type="appleArcade"/>
<colorBadge text="Enjoyed" type="enjoyed"/>
<colorBadge text="Kind of Enjoyed" type="kindEnjoyed"/>
<colorBadge text="Did not Enjoy" type="notEnjoyed"/>
<colorBadge text="Still Playing" type="stillPlaying"/>
<colorBadge text="Finished" type="finished"/>
<colorBadge text="Date" type="date"/>

```md
<colorBadge text="Playstation 4" type="ps4"/>
<colorBadge text="Xbox One" type="xbox"/>
<colorBadge text="Nintendo Switch" type="switch"/>
<colorBadge text="Apple Arcade" type="appleArcade"/>
<colorBadge text="Enjoyed" type="enjoyed"/>
<colorBadge text="Kind of Enjoyed" type="kindEnjoyed"/>
<colorBadge text="Did not Enjoy" type="notEnjoyed"/>
<colorBadge text="Still Playing" type="stillPlaying"/>
<colorBadge text="Finished" type="finished"/>
<colorBadge text="Date" type="date"/>
```

## Containers

::: tip Tip Heading
Text goes here.
:::

```md
::: tip Tip Heading
Text goes here.
:::
```

::: warning Warning Heading
Text goes here.
:::

```md
::: warning Warning Heading
Text goes here.
:::
```

::: danger Danger Heading
Text goes here.
:::

```md
::: danger Danger Heading
Text goes here.
:::
```

## Custom Container

::: right Heading
Text goes here.
:::

```md
::: right Heading
Text goes here.
:::
```

## Highlight

`Highlight`

```md
`Highlight`
```
